# python3

import sys
import time
import RPi.GPIO as GPIO
import threading
import numpy as np
from sensors import sensors
# from decisions import decisions
# from moves import moves


import cv2 as cv


from main_control import MainControl
main_control = MainControl()

GPIO.setmode(GPIO.BOARD)

motor_pin_list = [16, 12, 22, 15, 18, 13]
GPIO.setup(motor_pin_list, GPIO.OUT)

sensorsThread = False
# decisionsThread = False
# movementsThread = False

def main():
	print('Journey started!')
	time.sleep(1)
	main_control.tickInterval = 0.069
	
	GPIO.setup(11, GPIO.OUT)  # red
	GPIO.setup(7, GPIO.OUT)  # green
	
	print(cv.__version__)
	
	arr = np.array([1, 4, 7])
	print(np.__version__)
	print(arr)
	
	
	
	frame_width = 680
	frame_height = 480
	fps = 30.0
	
	video_capture = cv.VideoCapture(0)
	
	video_capture.set(cv.CAP_PROP_FRAME_WIDTH, frame_width)
	video_capture.set(cv.CAP_PROP_FRAME_HEIGHT, frame_height)
	
	size = (int(video_capture.get(cv.CAP_PROP_FRAME_WIDTH)), int(video_capture.get(cv.CAP_PROP_FRAME_HEIGHT)))
	
	fourcc = cv.VideoWriter_fourcc(*'MP4V')
	video_writer = cv.VideoWriter('sample.mp4v', fourcc, fps, size)
	
	i = 0
	
	while(video_capture.isOpened()):
		ret, frame = video_capture.read()
		
		# Image
		cv.imwrite('sample_' + str(i) + '.png', frame)
		gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
		cv.imshow('frame', gray)
		# i = i + 1
		
		
		# Video
		# video_writer.write(frame)
		# cv.imshow('frame', frame)
		
		key = cv.waitKey(1)
		if key & 0xFF == ord('q') or key == 27 or 'x' == chr(key & 255):
			break
			
	video_capture.release()
	video_writer.release()
	cv.destroyAllWindows()
	
	
	i = 0
	
	while i < 1:
		GPIO.output(11, True)
		time.sleep(1)
		GPIO.output(11, False)
		time.sleep(1)
		GPIO.output(7, True)
		time.sleep(1)
		GPIO.output(7, False)
		time.sleep(1)
		i+=1

	
	
	# sensorsThread = runThread("sensorsThread")
	# sensorsThread.start()
	
	# decisionsThread = runThread("decisionsThread")
	# decisionsThread.start()
	
	# movementsThread = runThread("movementsThread")
	# movementsThread.start()
	
	# while main_control.runSensors:
		# time.sleep(1)

	# close_threads([sensorsThread])
	GPIO.cleanup()
	print('Journey ended!')
	

def close_threads(threads):
	for thread in threads:
		if thread.name == 'sensorsThread':
			main_control.runSensors = False
		# if thread.name == 'decisionsThread':
			# main_control.runDecisions = False
		# if thread.name == 'movementsThread':
			# main_control.runMoves = False
		thread.join()
	
class runThread(threading.Thread):
	def __init__(self, name):
		threading.Thread.__init__(self)
		self.name = name
	def run(self):
		if self.name == "sensorsThread":
			sensors.start_sensors()
			sensors.run_sensors()
		# if self.name == "decisionsThread":
			# decisions.start_decisions()
			# decisions.run_decisions()
		# if self.name == "movementsThread":
			# moves.start_moves()
			# moves.run_moves()


def not_main():
	print('not main')
	
if __name__=='__main__':
	main()
else:
	not_main()
